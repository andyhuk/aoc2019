use strict;
use warnings;

use Data::Dumper;
use Math::Trig qw/pi/;

$::debug = 0;

my @asteroids;
my $Y = 0;
while (<DATA>) {
	chomp;
	my $X = 0;
	for (split //) {
		if ($_ eq '#') {
			push @asteroids, "$X,$Y";
		}
		$X++
	}
	$Y++;
}


sub between {
	my @path;
	my ($fromX, $fromY) = split /,/, $_[0];
	my ($toX, $toY) = split /,/, $_[1];

	my $deltaX = $toX - $fromX;
	my $deltaY = $toY - $fromY;

	#print " dX = $deltaX, dY = $deltaY: ";
	if ($deltaX < 0 && $deltaY < 0) {
		die "Only works on increasing positions\n";
	}

	if ($deltaY != 0) {
		for (sort ($fromY+1..$toY-1)) {
			my $xval = (($deltaX / $deltaY) * ($_ - $fromY)) + $fromX;
			
			push @path, "$xval,$_" unless ($xval != int($xval));
		}
	}
	else {
		for (sort ($fromX+1..$toX-1)) {
			my $yval = (($deltaY / $deltaX) * ($_ - $fromX)) + $fromY;
			push @path, "$_,$yval" unless ($yval != int($yval));
		}
	}
	print "$_[0] to $_[1] is dX$deltaX dY$deltaY @path\n" if $::debug;
	return @path;
}

sub can_see {
	my @asteroids = @_;
	my %locs = map { $_ => { count => 0 } } @asteroids;
	while (@asteroids > 1) {
		my $a = shift @asteroids;
		my $count = 0;
		for (@asteroids) {
			#print "Checking $a to $_\n";
			my @blocking = grep { exists $locs{$_} } between($a, $_);
			if (@blocking) {
				#print "$a to $_ blocked by @blocking\n";
				push @{$locs{$a}{blocked}}, $_;
				push @{$locs{$_}{blocked}}, $a;
				next;
			}
			$locs{$a}{count}++;
			push @{$locs{$a}{seen}}, $_;
			$locs{$_}{count}++;
			push @{$locs{$_}{seen}}, $a;
		}
	}
	return %locs;
}

my %locs = can_see(@asteroids);

my $max = (sort { $locs{$b}->{count} <=> $locs{$a}->{count} } keys %locs)[0];

my $count = scalar (keys %locs);
print "best of $count is $max\n";

my ($bestx, $besty) = split /,/, $max;

my $i = 1;
while (keys %locs > 1) {
	# Now sort the asteroids that have been seen in clockwise order
	# best is 29,28
	my @vape = sort { 
	    $a->[2] < 0 && $b->[2] >= 0 ? 1
	  : $a->[2] >= 0 && $b->[2] < 0 ? -1
	  : $a->[2] <=> $b->[2] 
	} 
	map { 
	  @_ = split /,/;
	  my ($xoff, $yoff) = ($_[0]-$bestx, $_[1]-$besty);
	  [$_, "$xoff,$yoff", atan2($xoff, -$yoff)] 
	} @{$locs{$max}->{seen}};

	printf "Vape list is %d long\n", scalar @vape;

	for (@vape) {
		my $coord = $_->[0];
		print "Found myself! - " if $coord eq $max;
		my $angle = $_->[2];
		print "$i $angle $coord\n";
		delete $locs{$coord};
		$i++;
	}
	@asteroids = sort { 
	  my @a = split /,/,$a;
	  my @b = split /,/,$b;
	  $a[0] <=> $b[0] || $a[1] <=> $b[1];
	} keys %locs;
	printf "New pass - %d left\n", scalar @asteroids;
	%locs = can_see(@asteroids);
}

print "Remaining is " . join " ", keys %locs;
print "\n";

__DATA__
.#......##.#..#.......#####...#..
...#.....##......###....#.##.....
..#...#....#....#............###.
.....#......#.##......#.#..###.#.
#.#..........##.#.#...#.##.#.#.#.
..#.##.#...#.......#..##.......##
..#....#.....#..##.#..####.#.....
#.............#..#.........#.#...
........#.##..#..#..#.#.....#.#..
.........#...#..##......###.....#
##.#.###..#..#.#.....#.........#.
.#.###.##..##......#####..#..##..
.........#.......#.#......#......
..#...#...#...#.#....###.#.......
#..#.#....#...#.......#..#.#.##..
#.....##...#.###..#..#......#..##
...........#...#......#..#....#..
#.#.#......#....#..#.....##....##
..###...#.#.##..#...#.....#...#.#
.......#..##.#..#.............##.
..###........##.#................
###.#..#...#......###.#........#.
.......#....#.#.#..#..#....#..#..
.#...#..#...#......#....#.#..#...
#.#.........#.....#....#.#.#.....
.#....#......##.##....#........#.
....#..#..#...#..##.#.#......#.#.
..###.##.#.....#....#.#......#...
#.##...#............#..#.....#..#
.#....##....##...#......#........
...#...##...#.......#....##.#....
.#....#.#...#.#...##....#..##.#.#
.#.#....##.......#.....##.##.#.##
