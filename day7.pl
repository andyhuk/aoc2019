# vi: ts=2 sw=2 sts=2 et
#
use strict;
use warnings;
use Data::Dumper;

my @initial = do {
  local $/;
  split /,/, (<DATA>);
};
my @input = (5);

sub intcode {
  my @input = @_;
  my @output = ();

  my @jump;
  my @opcodes;
  @opcodes[1..8,99] = (
    [3, sub { $_[3] = $_[1] + $_[2] } ],
    [3, sub { $_[3] = $_[1] * $_[2] } ],
    [1, sub { $_[1] = shift @input } ],
    [1, sub { push @output, $_[1] } ],
    [2, sub { push @jump, 1 if ($_[1]) } ],
    [2, sub { push @jump, 1 unless ($_[1]) } ],
    [3, sub { $_[3] = $_[1] < $_[2] ? 1 : 0} ],
    [3, sub { $_[3] = $_[1] == $_[2] ? 1 : 0} ],
    [0, sub { die "done\n" } ]
  );

  my $pc = 0;

  my @p = @initial;

  sub make_params {
    my ($modes, $p, @prm) = @_;
    my (@plist);

    for (@prm) {
      if ($modes % 10 == 1) {
        push @plist, $_;
      }
      else {
        push @plist, $p->[$_];
      }
      $modes = int($modes / 10);
    }
    return @plist;
  }

  sub apply_params {
    my ($modes, $p, $pos, @prm) = @_;

    for (@prm) {
      if ($modes % 10 == 0) {
        my $loc = $p->[$pos];
        $p->[$loc] = $_;
      }
      $modes = int($modes / 10);
      $pos++;
    }
  }

  while ($pc < @p) {
    my $o = $p[$pc] % 100;
    my $m = int($p[$pc] / 100);

    if ($o == 99) {
      last;
    } else {
      die "Bad opcode $o!" unless (defined $opcodes[$o]);
      my ($pcount, $sub) = @{$opcodes[$o]};

      my @plist = make_params($m, \@p, @p[$pc + 1 .. $pc + $pcount]);
      $sub->($o, @plist);
      apply_params($m, \@p, $pc + 1, @plist);

      if (@jump) {
        $pc = $plist[pop @jump];
      }
      else {
        $pc += $pcount + 1;
      }
    }
  }

  # print "Outputs:" . join(',', @output);

  return @output;
}

sub amplify {
  my $signal = 0;
  for my $phase (@_) {
    ($signal) = intcode($phase, $signal);
  }
  return $signal;
}

# Heap's algorithm - apparently
sub permute {
	sub heap {
		my $size = shift;
		if ($size == 1) {
			return [@_];
		}
		else {
			my @perms;
			for (my $i=0; $i < $size; $i++) {
				push @perms, heap($size-1, @_);
				# If odd, swap first & last
				if ($size % 2 == 1) {
					@_[0,$size-1] = @_[$size-1,0];
				}
				# If even, swap ith and last
				else {
					@_[$i,$size-1] = @_[$size-1, $i];
				}
			}
			return @perms;
		}
	}
	return heap(scalar @_, @_);
}

my @perms = permute(0..4);
my @calcs = sort { $b->{sig} <=> $a->{sig} } map { { seq => join(',',@$_), sig => amplify(@$_) } } @perms;

printf "did %d calculations\n", scalar @calcs;
print "Best is " . $calcs[0]->{seq} . " giving " . $calcs[0]->{sig} . "\n";

print "Example result is : " . amplify(qw/1 0 4 3 2/);

#print Dumper (\@calcs);

__DATA__
3,8,1001,8,10,8,105,1,0,0,21,34,51,68,89,98,179,260,341,422,99999,3,9,1001,9,4,9,102,4,9,9,4,9,99,3,9,1002,9,5,9,1001,9,2,9,1002,9,2,9,4,9,99,3,9,1001,9,3,9,102,3,9,9,101,4,9,9,4,9,99,3,9,102,2,9,9,101,2,9,9,1002,9,5,9,1001,9,2,9,4,9,99,3,9,102,2,9,9,4,9,99,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,99,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,2,9,4,9,99,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,99,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,101,2,9,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,99
