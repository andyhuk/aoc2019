# vi: ts=2 sw=2 sts=2 et
#
use strict;
use warnings;
use Data::Dumper;

my @initial = do {
  local $/;
  split /\s*,\s*/, (<DATA>);
};


my $machine_count = 0;

sub make_intcode {
  my @p = @initial;

  my @input = @_;
  my @output;

  my @jump;
  my @opcodes;
  my $pc = 0;
  @opcodes[1..8,99] = (
    [3, sub { $_[3] = $_[1] + $_[2] } ],
    [3, sub { $_[3] = $_[1] * $_[2] } ],
    [1, sub { if (@input) {
                $_[1] = shift @input;
              } else {
                # Yield!
                return -1;
              } } ],
    [1, sub { push @output, $_[1] } ],
    [2, sub { push @jump, 1 if ($_[1]) } ],
    [2, sub { push @jump, 1 unless ($_[1]) } ],
    [3, sub { $_[3] = $_[1] < $_[2] ? 1 : 0} ],
    [3, sub { $_[3] = $_[1] == $_[2] ? 1 : 0} ],
    [0, sub { die "done\n" } ]
  );

  my $machine_name = chr ($machine_count + ord 'A');
  $machine_count++;
  my $machine_state = 0;

  my $machine = sub {

    push @input, @_;
    @output = ();

    #print "Starting machine $machine_name - pc is $pc, inputs @input\n";

    sub make_params {
      my ($modes, $p, @prm) = @_;
      my (@plist);

      for (@prm) {
        if ($modes % 10 == 1) {
          push @plist, $_;
        }
        else {
          unless (defined $p->[$_]) {
            warn "Param $_ is undefined - max is " . (scalar @{$p} - 1);
          }
          push @plist, $p->[$_];
        }
        $modes = int($modes / 10);
      }
      return @plist;
    }

    sub apply_params {
      my ($modes, $p, $pos, @prm) = @_;

      for (@prm) {
        if ($modes % 10 == 0) {
          my $loc = $p->[$pos];
          $p->[$loc] = $_;
        }
        $modes = int($modes / 10);
        $pos++;
      }
    }

    while ($pc < @p) {
      my $o = $p[$pc] % 100;
      my $m = int($p[$pc] / 100);

      if ($o == 99) {
        $machine_state = -1;
        last;
      } else {
        die "Bad opcode $o!" unless (defined $opcodes[$o]);
        my ($pcount, $sub) = @{$opcodes[$o]};

#for (my $i = 0; $i < @p; $i++) {
#  print "$i : $p[$i]\n";
#}
        my @plist = make_params($m, \@p, @p[$pc + 1 .. $pc + $pcount]);
      #print "Running PC=$pc $o : @plist - inputs: @input, outputs: @output\n";
        if ($sub->($o, @plist) < 0) {
          # Asked to yield
          return (1, @output);
        }
        apply_params($m, \@p, $pc + 1, @plist);

        if (@jump) {
          $pc = $plist[pop @jump];
        }
        else {
          $pc += $pcount + 1;
        }
      }
    }
    return ($machine_state, @output);
  };
  return $machine;
}

sub amplify {
  my @amplifiers = map { make_intcode($_) } @_;

  my (@signals) = (0);
  my $state = 0;
  for (1..100) {
    for (@amplifiers) {
      #print "About to amplify @signals\n";
      ($state, @signals) = $_->(@signals);
      #print "Got $state @signals\n";
    }
    last if ($state < 1);
  }
  print "returning @signals\n";
  return $signals[0];
}

# Heap's algorithm - apparently
sub permute {
	sub heap {
		my $size = shift;
		if ($size == 1) {
			return [@_];
		}
		else {
			my @perms;
			for (my $i=0; $i < $size; $i++) {
				push @perms, heap($size-1, @_);
				# If odd, swap first & last
				if ($size % 2 == 1) {
					@_[0,$size-1] = @_[$size-1,0];
				}
				# If even, swap ith and last
				else {
					@_[$i,$size-1] = @_[$size-1, $i];
				}
			}
			return @perms;
		}
	}
	return heap(scalar @_, @_);
}

my @perms = permute(5..9);
my @calcs = sort { $b->{sig} <=> $a->{sig} } map { { seq => join(',',@$_), sig => amplify(@$_) } } @perms;

printf "did %d calculations\n", scalar @calcs;
print "Best is " . $calcs[0]->{seq} . " giving " . $calcs[0]->{sig} . "\n";

#my $foo = amplify(qw/9 7 8 5 6/);
#print "Example result is : $foo ";

#print Dumper (\@calcs);

__DATA__
3,8,1001,8,10,8,105,1,0,0,21,34,51,68,89,98,179,260,341,422,99999,3,9,1001,9,4,9,102,4,9,9,4,9,99,3,9,1002,9,5,9,1001,9,2,9,1002,9,2,9,4,9,99,3,9,1001,9,3,9,102,3,9,9,101,4,9,9,4,9,99,3,9,102,2,9,9,101,2,9,9,1002,9,5,9,1001,9,2,9,4,9,99,3,9,102,2,9,9,4,9,99,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,99,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,2,9,4,9,99,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,99,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,101,2,9,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,99

