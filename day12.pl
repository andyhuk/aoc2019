use strict;
use warnings;
use List::Util qw/sum reduce/;
use Data::Dumper;

sub calc_velocity {
	my ($moons, $i) = @_;
	my $count = scalar @{$moons};
	return if (($count - $i) < 1);

	for my $j ($i+1..$count-1) {
		for (0..2) {
			my $delta = 
			  ($moons->[$i]->{'pos'}->[$_] < $moons->[$j]->{'pos'}->[$_]) ? 1 :
			  ($moons->[$i]->{'pos'}->[$_] > $moons->[$j]->{'pos'}->[$_]) ? -1 : 0;
			$moons->[$i]->{vel}->[$_] += $delta;
			$moons->[$j]->{vel}->[$_] -= $delta;
		}
	}
	calc_velocity($moons, $i + 1);
}

my @moons;

while (<DATA>) {
	if (/x=(-?\d+), y=(-?\d+), z=(-?\d+)/) {
		push @moons, { 
			'pos' => [$1, $2, $3], 
			vel => [0, 0, 0]
		};
	}
}

my $upto = 1000;

for (1..$upto) {
	calc_velocity(\@moons, 0);
	for my $moon (@moons) {
		$moon->{'pos'}->[$_] += $moon->{vel}->[$_] for (0..2);
	}

}

print "After $upto steps\n";
for (@moons) {
	my (@pos) = @{$_->{'pos'}};
	my (@vel) = @{$_->{'vel'}};
	print "pos=<x=$pos[0], y=$pos[1], z=$pos[2]>, vel=<x=$vel[0], y=$vel[1], z=$vel[2]>\n";

	$_->{pot} = sum map { abs } @pos;
	$_->{kin} = sum map { abs } @vel;
}

my $total = sum map { $_->{pot} * $_->{kin} } @moons;
print "Total energy $total\n";

__DATA__
<x=3, y=3, z=0>
<x=4, y=-16, z=2>
<x=-10, y=-6, z=5>
<x=-3, y=0, z=-13>
