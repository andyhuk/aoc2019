# vi: et ts=2 sw=2 sts=2
use strict;
use warnings;


my @initial = split /,/, (<DATA>);
push @initial, (0) x 1000;


sub run_amplifiers {
  my @phases = @_;

  my @machines;
  my @comms;
  for (0..$#phases) {
    my $mnum = $_;
    my @input = ($phases[$_]);
    push @input, 0 if ($_ == 0);

    $comms[$_] = \@input;
    $machines[$_] = make_machine($_,
      sub {
        if (@input == 0) {
          print "$mnum : No input - running previous machine\n";
          if ($machines[$mnum-1]->() == 99) {
            print "$mnum-1 has ended\n";
          }
        }
        print "$mnum: input is @input\n";
        return shift @input;
      },
      sub { # push to output
        my $to_machine = $mnum + 1 % ($#phases - 1);
        print "$mnum : adding @_ to output (input of $to_machine)\n";
        push @{$comms[$to_machine]}, @_;
      }
    );
  }

  # Run the last machine
  for (1..3) {
    $machines[-1]->();
  }
}

run_amplifiers(6, 8, 7, 5, 9);

sub make_machine {
  my ($machine_name, $get_input, $send_output) = @_;

  my @p = @initial;
  my @jump;
  my $rel_pos = 0;
  my @opcodes;

  @opcodes[1..9,99] = (
    [3, sub { ${$_[3]} = ${$_[1]} + ${$_[2]} } ],
    [3, sub { ${$_[3]} = ${$_[1]} * ${$_[2]} } ],
    [1, sub { ${$_[1]} = $get_input->() } ],
    [1, sub { $send_output->(${$_[1]}) } ],
    [2, sub { push @jump, ${$_[2]} if (${$_[1]}) } ],
    [2, sub { push @jump, ${$_[2]} unless (${$_[1]}) } ],
    [3, sub { ${$_[3]} = ${$_[1]} < ${$_[2]} ? 1 : 0} ],
    [3, sub { ${$_[3]} = ${$_[1]} == ${$_[2]} ? 1 : 0} ],
    [1, sub { $rel_pos += ${$_[1]} } ],
    [0, sub { die "done\n" } ]
  );

  my $pc = 0;

  return sub {
    while ($pc < @p) {
      my $o = $p[$pc] % 100;
      my $m = int($p[$pc] / 100);

      if ($o == 99) {
        return 99;
      } else {
        die "Bad opcode $o!" unless (defined $opcodes[$o]);
        my ($pcount, $sub) = @{$opcodes[$o]};

        my @parms;
        for (@p[$pc + 1 .. $pc + $pcount]) {
          if ($m % 10 == 1) {
            my $c = $_;
            push @parms, \$c; #constant
          }
          elsif ($m % 10 == 2) {
            push @parms, \$p[$_ + $rel_pos];
          }
          else {
            push @parms, \$p[$_];
          }
          $m /= 10;
        }

        $sub->($o, @parms);

        if (@jump) {
          $pc = pop @jump;
        }
        else {
          $pc += $pcount + 1;
        }

        # Yield after output
        if ($o == 4) {
          return 0;
        }
      }
    }
  }
}

__DATA__
3,8,1001,8,10,8,105,1,0,0,21,34,51,68,89,98,179,260,341,422,99999,3,9,1001,9,4,9,102,4,9,9,4,9,99,3,9,1002,9,5,9,1001,9,2,9,1002,9,2,9,4,9,99,3,9,1001,9,3,9,102,3,9,9,101,4,9,9,4,9,99,3,9,102,2,9,9,101,2,9,9,1002,9,5,9,1001,9,2,9,4,9,99,3,9,102,2,9,9,4,9,99,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,99,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,2,9,4,9,99,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,99,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,101,2,9,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,99

