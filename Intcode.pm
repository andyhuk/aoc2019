# vi: et ts=2 sw=2 sts=2
package Intcode;

BEGIN {
  require Exporter;
  our $VERSION = 1.00;
  our @ISA = qw/Exporter/;
  our @EXPORT = qw/intcode make_intcode/;
}
use strict;
use warnings;

sub make_intcode {
  my ($initial, $get_input, $send_output, %options) = @_;

  my @jump;

  my $rel_pos = 0;
  my @opcodes;

  my $yield = 0;

  @opcodes[1..9,99] = (
    [3, sub { ${$_[3]} = ${$_[1]} + ${$_[2]} } ],
    [3, sub { ${$_[3]} = ${$_[1]} * ${$_[2]} } ],
    [1, sub { 
        $yield = 1 if ($options{yield_on_output});
        ${$_[1]} = $get_input->() } ],
    [1, sub { $send_output->(${$_[1]}); #$yield = 1 if ($options{yield_on_output}) 
      } ],
    [2, sub { push @jump, ${$_[2]} if (${$_[1]}) } ],
    [2, sub { push @jump, ${$_[2]} unless (${$_[1]}) } ],
    [3, sub { ${$_[3]} = ${$_[1]} < ${$_[2]} ? 1 : 0} ],
    [3, sub { ${$_[3]} = ${$_[1]} == ${$_[2]} ? 1 : 0} ],
    [1, sub { $rel_pos += ${$_[1]} } ],
    [0, sub { die "done\n" } ]
  );

  my $pc = 0;
  my @p = @{$initial};

  my $m = sub {
    my $ret = eval {
      while ($pc < @p) {
        my $o = $p[$pc] % 100;
        my $m = int($p[$pc] / 100);

        if ($o == 99) {
          last;
        } else {
          die "Bad opcode $o!" unless (defined $opcodes[$o]);
          my ($pcount, $sub) = @{$opcodes[$o]};

          my @parms;
          for (@p[$pc + 1 .. $pc + $pcount]) {
            if ($m % 10 == 1) {
              my $c = $_;
              push @parms, \$c; #constant
            }
            elsif ($m % 10 == 2) {
              die "Negative position!" unless ($_ + $rel_pos >= 0);
              $p[$_ + $rel_pos] = 0 unless defined $p[$_ + $rel_pos];
              push @parms, \$p[$_ + $rel_pos];
            }
            else {
              die "Negative position!" unless ($_ + $rel_pos >= 0);
              $p[$_] = 0 unless defined $p[$_];
              push @parms, \$p[$_];
            }
            $m /= 10;
          }

          $sub->($o, @parms);

          if (@jump) {
            $pc = pop @jump;
          }
          else {
            $pc += $pcount + 1;
          }

          if ($yield) {
            $yield = 0;
            #print "Yielding at pc=$pc\n";
            return 1;
          }
        }
      }
      print "Machine ending\n";
      return 0;
    };
    if ($@) {
      warn "Intcode error $@ - pc=$pc rel_pos=$rel_pos\n";
      warn "Dump : @p[$pc..($pc+8)]\n";
    }
    return $ret;
  };
  return $m;
}

sub intcode {
  my $m = make_intcode(@_);
  $m->();
}

1;

