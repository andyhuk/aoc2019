
# Part 1 - double digits
my $dd = join '|', map {"$_$_"} (0..9);
# Part 1 & 2 reject descending digits 
my $r = join '|', map { "$_\[0-" . ($_-1) . ']' } (3..9);

print scalar (grep /(?:$dd)/, grep !/(?:10|2[10]|$r)/, (246540..787419)); 
print "\n";

# Part 2 - double digits only (uses lookaround assertions)
my $ddo = join '|', map {"(?<!$_)$_$_(?!$_)"} (0..9);

print "RE1: (negated) is  !/(?:10|2[10]|$r)/\n";
print "RE2: is  /(?:$ddo)/\n";


print scalar (grep /(?:$ddo)/, grep !/(?:10|2[10]|$r)/, (246540..787419)); 
print "\n";
