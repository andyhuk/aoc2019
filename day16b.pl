
use strict;
use warnings;

my @pattern = (0, 1, 0, -1);

my $input1 = "03036732577212944063491565474664";
my $input= "59762770781817719190459920638916297932099919336473880209100837309955133944776196290131062991588533604012789279722697427213158651963842941000227675363260513283349562674004015593737518754413236241876959840076372395821627451178924619604778486903040621916904575053141824939525904676911285446889682089563075562644813747239285344522507666595561570229575009121663303510763018855038153974091471626380638098740818102085542924937522595303462725145620673366476987473519905565346502431123825798174326899538349747404781623195253709212209882530131864820645274994127388201990754296051264021264496618531890752446146462088574426473998601145665542134964041254919435635" ;

my $offset = substr $input, 0, 7;
my $dlen = length $input;

print 
 "Offset $offset,\n".
 "length $dlen\n"
 ;

# Length is 6500000 ($input * 10000)
# Offset is 5976277

# Now, pattern will be $offset x (0), $offset x (1)
#
# Sum ($offset % 650)
# + Sum (

my $input_len = length $input;
my $total_len = $input_len * 10000;
my $diff = $total_len - $offset;

my $offset_input = substr($input, ($offset % $input_len)) . $input x int($diff / $input_len);

if ($diff < (length($offset_input) / 2)) {
    printf "Diff is $diff, input len is %d\n", length $offset_input; 
	die "Can only work when offset is beyond half way";
}

sub apply_phase {
	my ($in, $start_pos) = @_;
	# Work backwards & keep running total
	my $last = length $in;
	my $tot = 0;
	my $j;
	for ($j = $last - 1; $j >= $start_pos; $j--) {
		$tot += substr($in, $j, 1);
		substr($in, $j, 1, $tot % 10);
	}
	return $in;
}

printf "At start we have %s\n", substr($offset_input, 0, 8);
for (0..99) {
	$offset_input = apply_phase($offset_input, 0);
	printf "After %d, we have %s\n", $_+1, substr($offset_input, 0, 8);
}

