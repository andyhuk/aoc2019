use strict;
use warnings;
use List::Util qw/sum reduce/;
use Data::Dumper;

sub calc_velocity {
	my ($moons, $i) = @_;
	my $count = scalar @{$moons};
	return if (($count - $i) < 1);

	for my $j ($i+1..$count-1) {
		for (0..2) {
			my $delta = 
			  ($moons->[$i]->{'pos'}->[$_] < $moons->[$j]->{'pos'}->[$_]) ? 1 :
			  ($moons->[$i]->{'pos'}->[$_] > $moons->[$j]->{'pos'}->[$_]) ? -1 : 0;
			$moons->[$i]->{vel}->[$_] += $delta;
			$moons->[$j]->{vel}->[$_] -= $delta;
		}
	}
	calc_velocity($moons, $i + 1);
}

my @moons;

while (<DATA>) {
	if (/x=(-?\d+), y=(-?\d+), z=(-?\d+)/) {
		push @moons, { 
			'pos' => [$1, $2, $3], 
			vel => [0, 0, 0]
		};
	}
}

sub calc_loop {
	#my @p = (3, 4, -10, -3);
	my ($axis, @p) = @_;
	my @v = (0, 0, 0, 0);

	my @perms = ([0,1],[0,2],[0,3],[1,2],[1,3],[2,3]);

	my @p_initial = @p;
	my @v_initial = @v;


	my $count = 0;

	OUTER: while (1) {
		for (@perms) {
			my ($a, $b) = @{$_};
			my $delta = ($p[$a] < $p[$b]) ? 1 : ($p[$a] > $p[$b]) ? -1 : 0;
			$v[$a] += $delta;
			$v[$b] -= $delta;
		}
		for (0..$#p) {
			$p[$_] += $v[$_];
		}
		$count++;
		# Check for match
		for (my $i = 0; $i < $#p_initial; $i++) {
			next OUTER if ($p_initial[$i] != $p[$i]);
			next OUTER if ($v_initial[$i] != $v[$i]);
		}
		last;
	}
	print "$axis @p @v Loops after $count iterations\n";
	return $count;
}

sub gcf {
  my ($x, $y) = @_;
  ($x, $y) = ($y, $x % $y) while $y;
  return $x;
}

sub lcm {
  return($_[0] * $_[1] / gcf($_[0], $_[1]));
}

sub multigcf {
  my $x = shift;
  $x = gcf($x, shift) while @_;
  return $x;
}

sub multilcm {
  my $x = shift;
  $x = lcm($x, shift) while @_;
  return $x;
}

my @loops = (
calc_loop('X', 3, 4, -10, -3),
calc_loop('Y', 3, -16, -6, 0),
calc_loop('Z', 0, 2, 5, -13));

my $lcm = multilcm(@loops);
print "LCM is $lcm\n";


#calc_vel_2(100, @moons);
die "done";


my %states;

my $upto = 0;
my $ix = join ',', map { (@{$_->{'pos'}}, @{$_->{vel}}) } @moons;
while (1) {
	$states{$ix} = $upto;

	calc_velocity(\@moons, 0);
	for my $moon (@moons) {
		$moon->{'pos'}->[$_] += $moon->{vel}->[$_] for (0..2);
	}
	$upto++;
	
	$ix = join ',', map { (@{$_->{'pos'}}, @{$_->{vel}}) } @moons;
	if (exists $states{$ix}) {
		print "Found matching state $ix after $upto - same as $states{$ix}\n";
		last;
	}
}

print "After $upto steps\n";
for (@moons) {
	my (@pos) = @{$_->{'pos'}};
	my (@vel) = @{$_->{'vel'}};
	print "pos=<x=$pos[0], y=$pos[1], z=$pos[2]>, vel=<x=$vel[0], y=$vel[1], z=$vel[2]>\n";

	$_->{pot} = sum map { abs } @pos;
	$_->{kin} = sum map { abs } @vel;
}

my $total = sum map { $_->{pot} * $_->{kin} } @moons;
print "Total energy $total\n";

__DATA__
<x=3, y=3, z=0>
<x=4, y=-16, z=2>
<x=-10, y=-6, z=5>
<x=-3, y=0, z=-13>
