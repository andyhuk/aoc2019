use strict;
use warnings;
use List::Util qw/sum/;
use Data::Dumper;
use POSIX qw/ceil/;

# Top down or bottom up?

my %f;
while (<DATA>) {
	chomp;
	my ($i,$o) = split /\s*=>\s*/;
	$o =~ /(\d+)\s+(\w+)/; 
	$f{$2} = { formula => [$1, split/,\s*/, $i] };
}


sub produce {
	my ($q, $c) = @_;

	return 0 if ($q == 0);
	if ($c eq 'ORE') {
#		print "Need $q ORE\n";
		return $q;
	}
#	print "Producing $q, $c\n";

	my ($out_amt, @inputs) = @{$f{$c}->{formula}};

	my $minqty = $out_amt * (ceil($q / $out_amt));
#	print "Will produce $minqty of $c (need $q)\n";
	my $excess = $minqty - $q;

	$f{$c}->{excess} += $excess;

	my $ore = 0;
	for (@inputs) {
		my ($qq, $cc) = split /\s+/;
		my $need_amt = int($minqty * $qq / $out_amt);

#	print "need $need_amt of $cc\n";
		
		# Consume excess
		if ($f{$cc}->{excess}) {
			if ($need_amt > $f{$cc}->{excess}) {
				$need_amt -= $f{$cc}->{excess};
				$f{$cc}->{excess} = 0;
			}
			else {
				$f{$cc}->{excess} -= $need_amt;
				$need_amt = 0;
			}
		}
		$ore += produce($need_amt, $cc);
	}

	return $ore;
}

my $for_one = 216477;
my $capacity = 1000000000000;

my $try = int($capacity / $for_one);


for (1..2000) {
	# Reset %f
	for (values %f) { $_->{excess} = 0 }

	my $ore1 = produce($try, "FUEL");
	my $diff = $capacity - $ore1;
	print "$try requires $ore1 ORE remaining $diff\n";

	for (values %f) { $_->{excess} = 0 }
	my $ore2 = produce($try+1, "FUEL");
	print "$try+1 requires $ore2 ORE \n";

	last if ($ore1 < $capacity && $ore2 > $capacity);

	my $multiplier = ($capacity + $diff) / $capacity ;
	print "will multiply by $multiplier\n"; 
	my $prevtry = $try;
	$try = int($try * $multiplier) ;

	if ($try == $prevtry) {
		$try += ($diff <=> 0);
	}
} 

__DATA__
1 HVXJL, 1 JHGQ => 2 ZQFQ
6 GRQTX => 6 VZWRS
128 ORE => 2 GRQTX
1 MJPSW => 4 MGZBH
3 HLQX => 8 KSMW
4 QLNS => 9 LFRW
10 HBCN => 3 CZWP
1 CQRJP => 9 MJPSW
1 SLXC => 6 SDTGP
1 MTGVK => 4 NZWLQ
4 PMJX => 3 CVKM
2 LDKGL, 2 SFKF => 5 XZDV
1 QLNS, 1 VZWRS => 5 RSBT
1 NRQS, 22 LQFDM => 4 PMJX
17 XZDV, 8 GSRKQ => 3 ZGDC
11 BPJLM, 18 ZGDC, 1 JHGQ => 5 BXNJX
2 GRQTX, 1 CQRJP => 7 NRQS
1 LJTL => 7 DBHXK
15 HPBQ, 5 PSPCF, 1 JHGQ, 25 ZMXWG, 1 JTZS, 1 SDTGP, 3 NLBM => 6 MQVLS
9 KSMW => 2 GXTBV
3 HVXJL => 5 JHGQ
1 ZWXT, 13 MJPSW, 10 HVXJL => 5 LDKGL
1 GRQTX => 2 LQFDM
190 ORE => 5 FQPNW
1 GTQB => 9 HVHN
1 TNLN, 9 HVHN, 1 WLGT, 4 NZMZ, 2 QTPC, 1 LPTF => 7 WFCH
3 PMJX => 5 SFKF
1 ZGDC => 9 HTVR
193 ORE => 1 CQRJP
1 BPJLM, 1 HPBQ, 3 HVHN => 6 NLBM
2 SFKF => 1 GSRKQ
1 ZGDC => 8 GTQB
1 LSPMR, 53 LDKGL, 24 WFCH, 32 GDLH, 2 HLQX, 14 NLBM, 18 BDZK, 7 MDSRW, 9 MQVLS => 1 FUEL
12 SFKF => 7 NZMZ
13 PVJM => 3 XBTH
7 GSRKQ, 7 LPTF, 1 HLQX, 1 FJHK, 1 DHVM, 3 SFKF, 15 NLBM, 2 SDTGP => 3 LSPMR
4 LFRW, 28 MJPSW => 4 GDLH
6 VZWRS, 8 MJPSW => 8 HVXJL
13 LFRW => 4 ZWQW
1 LQFDM, 7 NZWLQ, 2 HVXJL => 4 HLQX
2 KSMW, 1 WDGN, 4 ZQFQ => 1 ZMXWG
3 MGZBH => 2 LPTF
1 LFRW, 1 CVKM, 3 LDKGL => 4 LJTL
3 LJTL, 20 CZWP, 1 HPBQ => 9 WLGT
3 FQPNW => 8 MTGVK
1 MTDWJ, 1 CVKM => 9 WDGN
5 ZWQW => 3 MTDWJ
2 CVKM => 8 QTPC
2 PVJM, 9 ZWQW, 1 MTDWJ => 4 HBCN
5 RSBT, 2 WDGN, 6 GSRKQ => 1 BPJLM
34 JHGQ, 6 ZGDC => 8 DHVM
3 QTPC, 1 RSBT, 1 GXTBV => 9 JTZS
1 BXNJX, 2 JTZS => 5 SLXC
23 LPTF, 2 NZMZ => 4 TNLN
24 HTVR, 5 DBHXK => 2 FJHK
5 LPTF, 5 QTPC => 4 PSPCF
17 MTGVK, 27 LQFDM => 4 QLNS
1 CVKM, 5 HTVR => 8 HPBQ
6 ZQFQ, 28 XBTH => 7 MDSRW
13 WDGN => 5 BDZK
1 MJPSW, 2 VZWRS => 4 ZWXT
1 MGZBH, 1 GRQTX => 8 PVJM
