use strict;
use warnings;

use Data::Dumper;

$::debug = 0;

my @asteroids;
my $Y = 0;
while (<DATA>) {
	chomp;
	my $X = 0;
	for (split //) {
		if ($_ eq '#') {
			push @asteroids, "$X,$Y";
		}
		$X++
	}
	$Y++;
}

my %locs = map { $_ => { count => 0 } } @asteroids;

sub between {
	my @path;
	my ($fromX, $fromY) = split /,/, $_[0];
	my ($toX, $toY) = split /,/, $_[1];

	my $deltaX = $toX - $fromX;
	my $deltaY = $toY - $fromY;

	#print " dX = $deltaX, dY = $deltaY: ";
	if ($deltaX < 0 && $deltaY < 0) {
		die "Only works on increasing positions\n";
	}

	if ($deltaY != 0) {
		for (sort ($fromY+1..$toY-1)) {
			my $xval = (($deltaX / $deltaY) * ($_ - $fromY)) + $fromX;
			
			push @path, "$xval,$_" unless ($xval != int($xval));
		}
	}
	else {
		for (sort ($fromX+1..$toX-1)) {
			my $yval = (($deltaY / $deltaX) * ($_ - $fromX)) + $fromY;
			push @path, "$_,$yval" unless ($yval != int($yval));
		}
	}
	print "$_[0] to $_[1] is dX$deltaX dY$deltaY @path\n" if $::debug;
	return @path;
}

while (@asteroids > 1) {
	my $a = shift @asteroids;
	my $count = 0;
	for (@asteroids) {
		#print "Checking $a to $_\n";
		my @blocking = grep { exists $locs{$_} } between($a, $_);
		if (@blocking) {
			#print "$a to $_ blocked by @blocking\n";
			push @{$locs{$a}{blocked}}, $_;
			push @{$locs{$_}{blocked}}, $a;
			next;
		}
		$locs{$a}{count}++;
		push @{$locs{$a}{seen}}, $_;
		$locs{$_}{count}++;
		push @{$locs{$_}{seen}}, $a;
	}
}

for (sort keys %locs) {
	my $count = $locs{$_}{count};
	#print "$_ can see $count\n";
}

# 1,2 to 3,4 blocked by 4,3 - WRONG!
my $max = (sort { $locs{$b}->{count} <=> $locs{$a}->{count} } keys %locs)[0];

print "best is $max\n";
print Dumper \$locs{$max};

__DATA__
.#......##.#..#.......#####...#..
...#.....##......###....#.##.....
..#...#....#....#............###.
.....#......#.##......#.#..###.#.
#.#..........##.#.#...#.##.#.#.#.
..#.##.#...#.......#..##.......##
..#....#.....#..##.#..####.#.....
#.............#..#.........#.#...
........#.##..#..#..#.#.....#.#..
.........#...#..##......###.....#
##.#.###..#..#.#.....#.........#.
.#.###.##..##......#####..#..##..
.........#.......#.#......#......
..#...#...#...#.#....###.#.......
#..#.#....#...#.......#..#.#.##..
#.....##...#.###..#..#......#..##
...........#...#......#..#....#..
#.#.#......#....#..#.....##....##
..###...#.#.##..#...#.....#...#.#
.......#..##.#..#.............##.
..###........##.#................
###.#..#...#......###.#........#.
.......#....#.#.#..#..#....#..#..
.#...#..#...#......#....#.#..#...
#.#.........#.....#....#.#.#.....
.#....#......##.##....#........#.
....#..#..#...#..##.#.#......#.#.
..###.##.#.....#....#.#......#...
#.##...#............#..#.....#..#
.#....##....##...#......#........
...#...##...#.......#....##.#....
.#....#.#...#.#...##....#..##.#.#
.#.#....##.......#.....##.##.#.##
